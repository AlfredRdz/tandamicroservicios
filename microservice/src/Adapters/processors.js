const Service = require('../Services');
const { InternalError } = require('../settings');

const { queueCreate, queueUpdate, queueDelete, queueFindOne, queueView } = require('./index');

async function View(job, done) {

    try {
        const {  } = job.data;

        console.log(job.id)

        let { statusCode, data, message} = await Service.View({ });

        done(null, { statusCode, data, message });
    } catch (error) {
        console.log({ step: 'adapter queueView', error: error.toString() })

        done(null, { statusCode: 500, message: InternalError})
    }

}

async function Create(job, done) {

    try {
        const { name, age, color } = job.data;

        let { statusCode, data, message} = await Service.Create({ age, color, name});

        done(null, { statusCode, data, message });
    } catch (error) {
        console.log({ step: 'adapter queueCreate', error: error.toString() })

        done(null, { statusCode: 500, message: InternalError})
    }

}

async function Update(job, done) {

    try {
        const { name, age, color, id } = job.data;

        let { statusCode, data, message} = await Service.Update({ age, color, name, id});

        done(null, { statusCode, data, message });
    } catch (error) {
        console.log({ step: 'adapter queueUpdate', error: error.toString() })

        done(null, { statusCode: 500, message: InternalError})
    }

}

async function Delete(job, done) {

    try {
        const { id } = job.data;

        let { statusCode, data, message} = await Service.Delete({ id });

        done(null, { statusCode, data, message });
    } catch (error) {
        console.log({ step: 'adapter queueDelete', error: error.toString() })

        done(null, { statusCode: 500, message: InternalError})
    }

}

async function FindOne(job, done) {

    try {
        const { id } = job.data;

        let { statusCode, data, message} = await Service.FindOne({ id });

        done(null, { statusCode, data, message });
    } catch (error) {
        console.log({ step: 'adapter queueFindOne', error: error.toString() })

        done(null, { statusCode: 500, message: InternalError})
    }

}

async function run() {
    try {
        console.log('vamos a inicializar worker')
        queueView.process(View) 
        queueCreate.process(Create)
        queueDelete.process(Delete)
        queueFindOne.process(FindOne)
        queueUpdate.process(Update)

    } catch (error) {
        console.log(error)
    }
}


module.exports = { View, Create, Update, Delete, FindOne, run }