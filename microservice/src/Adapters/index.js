const bull = require('bull');

const { redis } = require('../settings');

const ops = { redis: { host: redis.host, port: redis.port } };

const queueCreate = bull("curso:create", ops)
const queueUpdate = bull("curso:update", ops)
const queueDelete = bull("curso:delete", ops)
const queueFindOne = bull("curso:findOne", ops)
const queueView = bull("curso:view", ops)


module.exports = { queueCreate, queueUpdate, queueDelete, queueFindOne, queueView };