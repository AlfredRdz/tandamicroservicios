const dotenv = require('dotenv');
const { Sequelize } = require('sequelize');

dotenv.config();

const redis = { host: process.env.REDIS_HOST, port: process.env.REDIS_PORT };

const InternalError = 'No podemos procesar tu solicitud en estos momentos';

const sequelize = new Sequelize({  
    host: process.env.POSTGRESQL_HOST, 
    port: process.env.POSTGRESQL_PORT,
    database: process.env.POSTGRESQL_DB,
    username: process.env.POSTGRESQL_USER, 
    password: process.env.POSTGRESQL_PASS,
    dialect: 'postgres'
})

module.exports = { redis, InternalError, sequelize };