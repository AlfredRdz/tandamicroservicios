const { Model } = require('../Models/index');

async function Create({ name, age, color }){
    try {

        let instancia = await Model.create(
            { name, age, color }, 
            { fields: ['name', 'age', 'color'], logging: false } 
        );

        return { statusCode: 200, data: instancia.toJSON() }

    } catch (error) {
        console.log({step: 'controller Create', error: error.toString() })

        return { statusCode: 400, mensaje: error.toString() }
    }
}

async function FindOne({ where = {} }){
    try {

        let instancia = await Model.findOne( { where, logging: false });

        if (instancia) {
            return { statusCode: 200, data: instancia.toJSON() }
        } else {
            return { statusCode: 400, message: "No existe el usuario" }
        }

        

    } catch (error) {
        console.log({step: 'controller Create', error: error.toString() })

        return { statusCode: 400, message: error.toString() }
    }
}

async function Delete({ where = {} }){
    try {

        let instancia = Model.destroy({ where, logging: false });

        return { statusCode: 200, data: instancia.toJSON() }

    } catch (error) {
        console.log({step: 'controller Create', error: error.toString() })

        return { statusCode: 400, message: error.toString() }
    }
}

async function Update({ name, age, color, id }){
    try {

        let instancia = await Model.update( 
            { name, age, color }, 
            { where: { id }, logging: false, returning: true });

        return { statusCode: 200, data: instancia[1][0].toJSON() }

    } catch (error) {
        console.log({step: 'controller Create', error: error.toString() })

        return { statusCode: 200, mensaje: error.toString() }
    }
}

async function View({ where = {} }){
    try {

        let instancia = await Model.findAll( { where, logging: false });

        return { statusCode: 200, data: instancia }

    } catch (error) {
        console.log({step: 'controller Create', error: error.toString() })

        return { statusCode: 200, mensaje: error.toString() }
    }
}

module.exports = { Create, FindOne, Delete, Update, View }