const Controllers = require('../Controllers');
const { InternalError } = require('../settings');

async function Create({ age, color, name}) {

    try {

        let { statusCode, data, message } = await Controllers.Create({ age, color, name})

        return { statusCode, data, message };


    } catch (error) {
        console.log({ step: 'servicio Create', error: error.toString() })

        return { statusCode: 500, message: error.toString() };
    }  

}

async function Update({ age, color, name, id}) {

    try {

        let { statusCode, data, message } = await Controllers.Update({ age, color, name, id})

        return { statusCode, data, message };


    } catch (error) {
        console.log({ step: 'servicio Update', error: error.toString() })

        return { statusCode: 500, message: error.toString() };
    }  
    
}

async function Delete({ id }) {

    try {

        const findOne = await Controllers.FindOne({ where: { id }});

        if (findOne.statusCode !== 200) {
            /*let response = {
                400: { statusCode: 400, message: "No existe el usuario a eliminar"},
                500: { statusCode: 500, message: InternalError }
            }*/

            switch (findOne.statusCode) {
                case 400: return { statusCode: 400, message: "No existe el usuario a eliminar"}
                case 500: return { statusCode: 500, message: InternalError }
            
                default:
                    return { statusCode: findOne.statusCode, message: findOne.message}
                    break;
            }

            return response[findOne.statusCode];
        }

        const del = await Controllers.Delete({ where: {id}})

        if (del.statusCode === 200) {
            return { statusCode: 200, data: findOne.data};
        } else { 
            return { statusCode: 400, message: InternalError };
        }

    } catch (error) {
        console.log({ step: 'servicio Delete', error: error.toString() })

        return { statusCode: 500, message: error.toString() };
    }  
    
}

async function FindOne({ id }) {

    try {

        let { statusCode, data, message } = await Controllers.FindOne({ where: { id }})

        return { statusCode, data, message };


    } catch (error) {
        console.log({ step: 'servicio FindOne', error: error.toString() })

        return { statusCode: 500, message: error.toString() };
    }  
    
}

async function View({  }) {

    try {

        let { statusCode, data, message } = await Controllers.View({ })

        return { statusCode, data, message };


    } catch (error) {
        console.log({ step: 'servicio View', error: error.toString() })

        return { statusCode: 500, message: error.toString() };
    }  
    
}

module.exports = { Create, FindOne, Delete, Update, View }