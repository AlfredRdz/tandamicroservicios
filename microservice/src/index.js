const { SyncDB } = require('./Models');
const { run } = require('./Adapters/processors');

module.exports = { SyncDB, run }