const bull = require('bull');

const redis = { host: '192.168.0.100', port: 6379 };

const ops = { redis: { host: redis.host, port: redis.port } };

const queueCreate = bull("curso:create", ops)
const queueUpdate = bull("curso:update", ops)
const queueDelete = bull("curso:delete", ops)
const queueFindOne = bull("curso:findOne", ops)
const queueView = bull("curso:view", ops)

async function Create({ name, age, color }) {
    try {
        const job = await queueCreate.add({ name, age, color })

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message } 
    } catch (error) {
        console.log(error)
    }   
}

async function Update({ name, age, color, id }) {
    try {
        const job = await queueUpdate.add({ name, age, color, id })

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message } 
    } catch (error) {
        console.log(error)
    }   
}

async function Delete({ id }) {
    try {
        const job = await queueDelete.add({ id })

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message } 
    } catch (error) {
        console.log(error)
    }   
}

async function FindOne({ id }) {
    try {
        const job = await queueFindOne.add({ id })

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message } 

    } catch (error) {
        console.log(error)
    }   
}

async function View({  }) {
    try {
        const job = await queueView.add({})

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message } 
    } catch (error) {
        console.log(error)
    }   
}

async function main() {
    //await Create({name: 'alfred', age: 23, color: 'blue'});
    
    //await Delete({ id: 4 })

    //await Update({name: 'alfred', age: 24, id: 5 });

    //await FindOne({ id: 5})

    //await View({});

    
}

module.exports = { Create, Update, Delete, FindOne, View }
